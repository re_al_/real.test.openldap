﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Text;

namespace ReAl.Test.OpenLdap
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var baseOfSearch = "dc=integrate,dc=com,dc=bo";
                var ldapHost = "192.168.0.101";
                var ldapPort = 389;
                var connectAsDN = "cn=admin,dc=integrate,dc=com,dc=bo";
                var pageSize = 1000;
                var secureString = "Desa2016@";

                var openLDAPHelper = new LDAPHelper(
                    baseOfSearch,
                    ldapHost,
                    ldapPort,
                    AuthType.Basic,
                    connectAsDN,
                    secureString,
                    pageSize);

                var searchFilter = "objectclass=posixAccount";
                //var searchFilter = "uid=rvera";
                var attributesToLoad = new[] { "sn","uid","cn","userPassword" };
                var pagedSearchResults = openLDAPHelper.PagedSearch(
                    searchFilter,
                    attributesToLoad);

                foreach (var searchResultEntryCollection in pagedSearchResults)
                    foreach (SearchResultEntry searchResultEntry in searchResultEntryCollection)
                    {
                        Console.WriteLine(searchResultEntry.Attributes["uid"][0] + ": " +
                                          searchResultEntry.Attributes["cn"][0]);
                        Console.WriteLine(searchResultEntry.Attributes["userPassword"][0]);
                        Console.WriteLine(".......");
                    }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
                Console.WriteLine(exp.StackTrace);
            }

            Console.WriteLine("Presione una tecla para terminar...");
            Console.Read();
        }
    }
}
